from kivy.app import App
from kivy.clock import *
from kivy.uix.image import Image
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.scatterlayout import  ScatterLayout
from kivy.uix.relativelayout import  RelativeLayout

from kivy.graphics import *
from kivy.lang import Builder

from kivy.animation import Animation
from kivy.core.window import Window
from kivy.properties import NumericProperty, ReferenceListProperty
from math import atan, degrees, cos, sin, asin, pi

import gc
from random import randrange as rnd
import random

Builder.load_string('''
<Obj>:
    canvas.before:
        PushMatrix
        Rotate:
            angle: self.angle
            axis: (0, 0, 1)
            origin: self.center
    canvas.after:
        PopMatrix
 ''')

class Obj(Image):
    angle = NumericProperty(0)
    def __init__(self, root):
        super(Obj,self).__init__()
        self.root = root
        root.add_widget(self)

    def move_scene(self, dx = 0, dy = 0):
        self.x += dx    
        self.y += dy    
        
    def kill(self, *args):
        self.root.remove_widget(self)
        del(self)
        
            
class Monster(Obj):
    damage = NumericProperty(1)
    vx = NumericProperty(0)
    vy = NumericProperty(0)
    v = NumericProperty(1.5)
    da = NumericProperty(0)
    
    def change_angle(self, *args):
        ch = rnd(3)
        self.da = 90
        if ch == 1:
            self.da = -90
        elif ch == 2:
            self.da = 0
        Clock.schedule_once(self.change_angle, random.random()*1.5)
        
    
    def __init__(self,root,target):
        super(Monster,self).__init__(root)
        self.source = 'monster.png'
        self.size = (24,24)
        self.target = target
        self.root = root
        Clock.schedule_once(self.change_angle, 0.1)
        
    def move(self):
        #~ pass
        
        x,y = self.target.center
        x = self.center_x - x
        y = self.center_y - y
        h = (x**2 + y**2)**0.5
        if h:
            self.angle = asin(y/h)
            if x < 0:
                self.angle = pi  - self.angle
        self.angle = self.angle * 180 / pi
          
        self.dvx = self.v * cos(self.da * pi/ 180)
        self.dvy = self.v * sin(self.da * pi/ 180)

        self.vx = - self.v * cos(self.angle * pi/ 180)
        self.vy = - self.v * sin(self.angle * pi/ 180)
        
        self.x += self.vx
        self.y += self.vy
#~ 
        self.x += self.dvx
        self.y += self.dvy
        
    def kill(self):
        try:
            self.root.monsters.remove(self)
        except:
            pass
        super(Monster,self).kill()
        del(self)
        
        
class Bullet(Obj):
    damage = NumericProperty(1)
    vx = NumericProperty(0)
    vy = NumericProperty(0)
    v = NumericProperty(25)
    def __init__(self, center, angle, root):
        super(Bullet,self).__init__(root)
        self.source = 'bullet.png'
        self.angle = angle+180
        self.root = root
        self.pos = center
        self.size = (8,2)
        self.vx = self.v * cos(self.angle * pi/ 180)
        self.vy = self.v * sin(self.angle * pi/ 180)
        Clock.schedule_once(self.kill, 1.5)
        print(self.pos)
        

    def move(self):
        self.x += self.vx
        self.y += self.vy
        
        for mon in self.root.monsters:
            if self.collide_widget(mon):
                self.kill()
                mon.kill()
                break
        #~ 
        
    def kill(self, *args):
        try:
            self.root.bullets.remove(self)
        except:
            pass
        super(Bullet,self).kill()
        del(self)


        
class Man(Obj):
    def __init__(self, root):
        super(Man,self).__init__(root)
        self.source = 'solder.zip'
        self.size = (48,88)
        self.vx = 3
        self.vy = 3
        self.keys = set()
        #self.pos = (center_pos[0]+h,center_pos[1])
        #self.center_pos = center_pos
        self.root = root
        
        
        
    def move(self):
        if 'left' in self.keys:
            self.x -= self.vx
        if 'right' in self.keys:
            self.x += self.vx
        if 'up' in self.keys:
            self.y += self.vy
        if 'down' in self.keys:
            self.y -= self.vy
        self.targeting()
        

    def fire(self):
        b = Bullet(self.center, self.angle,self.root)
        self.root.bullets += [b]
        
    def bersek(self, dt):
        self.angle += 3 
        self.fire()

    def on_touch_down(self, touch):
        #Clock.schedule_interval(self.bersek, 0.02)
        self.fire()
        
    
            
    def targeting(self):
        x,y = Window.mouse_pos
        x = self.center_x - x
        y = self.center_y - y
        h = (x**2 + y**2)**0.5
        if h:
            self.angle = asin(y/h)
            if x < 0:
                self.angle = pi  - self.angle
        self.angle = self.angle * 180 / pi  
        


class MyApp(App):
    def build(self):
        #gc.disable()
        root = self.root = RelativeLayout()
        root.bullets = []
        #self.m2 = MyApp(scene)

        rt = Obj(root)
        rt.source = "bg.png"
        rt.size = (2000,2000)
        #rt.pos = (-100,-100)
        root.relocations = [rt]
        self.root.monsters = []
        
        self.m1 = Man(root)
        self.m1.pos = (-100,-100)
        
        #root.add_widget(self.m2)
        self.target = Image()
        self.target.source = 'bullet.png'
        root.add_widget(self.target)

        
        self._keyb = Window.request_keyboard(self._keyb_closed,self)
        self._keyb.bind(on_key_down  = self._on_key_down)
        self._keyb.bind(on_key_up = self._on_key_up)
        
        Clock.schedule_interval(self.move, 1 / 25.0)
        Clock.schedule_interval(self.new_mon, 0.5)
        #Clock.schedule_interval(self.reflec, 3)
        
        self.new_mon()
        
        return root
    

    def new_mon(self, *args):
        d = Monster(self.root, self.m1)
        d.pos = (rnd(100), rnd(400))
        self.root.monsters.append(d)
        
    def reflec(self, *args):
        counts = {}
        for obj in gc.get_objects():
            tn = type(obj).__name__
            try:
                counts[tn] += 1
            except:
                counts[tn] = 1
        for tn in counts:
            if tn in ['Bullet','Monster']:
                print(tn,counts[tn])
        print('---')
        print (len(self.root.bullets))
        print (len(self.root.monsters))
         
    def scene_center(self, obj):

        dx = obj.x - 600
        if dx > 0:
            obj.x -= dx
        else:
            dx = obj.x - 50
            if dx < 0:
                obj.x -= dx
            else:
                dx = 0

        dy = obj.y - 600
        if dy > 0:
            obj.y -= dy
        else:
            dy = obj.y - 50
            if dy < 0:
                obj.y -= dy
            else:
                dy = 0
        if dy or dx: 
            for obj in self.root.bullets:
                obj.move_scene(-dx, -dy)
                
            for obj in self.root.relocations:
                obj.move_scene(-dx, -dy)
                
            for mon in self.root.monsters:
                mon.move_scene(-dx, -dy)
                
    def move(self, *args):
        self.m1.move()
        self.scene_center(self.m1)      
        for bullet in self.root.bullets:
            bullet.move()
        
        for mon in self.root.monsters:
            mon.move()
        
        self.target.center = Window.mouse_pos
    
    def _on_key_down(self,keyboard, keycode, text, modifiers):
        self.m1.keys.add(keycode[1])

    def _on_key_up(self,keyboard, keycode, *largs):
        self.m1.keys.remove(keycode[1])
    
    def _keyb_closed(*args):
        print('qu-qu')


            
            


if __name__ == '__main__':
    MyApp().run()


