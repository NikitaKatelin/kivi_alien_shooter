
from kivy.app import App
from kivy.clock import *
from kivy.uix.image import Image
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.scatterlayout import  ScatterLayout
from kivy.uix.relativelayout import  RelativeLayout
from kivy.uix.floatlayout import  FloatLayout
from kivy.core.audio import SoundLoader

from kivy.graphics import *
from kivy.lang import Builder

from kivy.animation import Animation
from kivy.core.window import Window
from kivy.properties import NumericProperty, ReferenceListProperty
from math import atan, degrees, cos, sin, asin, pi

import gc
from random import randrange as rnd
import random
import time

Builder.load_string('''
<Obj>:
	canvas.before:
		PushMatrix
		Rotate:
			angle: self.angle
			axis: (0, 0, 1)
			origin: self.center
	canvas.after:
		PopMatrix

<Monster>:
    canvas.after:
        Color:
            rgba: (1.5-self.hp/self.hp_max,self.hp/self.hp_max,0,0.3)
        Rectangle
            pos: (self.center_x-self.hpbar_color_width/2, self.y + self.height*0.8)
            size: (self.hpbar_color_width*self.hp/self.hp_max,5)
 ''')


class Obj(Image):
	angle = NumericProperty(0)
	def __init__(self, root):
		super(Obj,self).__init__()
		self.root = root
		self.size_hint = (None,None)
		root.add_widget(self)
	   
	def kill(self, *args):
		self.root.remove_widget(self)
		del(self)
		
			
class Monster(Obj):
	damage = {'damage':25}
	vx = NumericProperty(0)
	vy = NumericProperty(0)
	v = NumericProperty(1.5)
	da = NumericProperty(0)
	hp = NumericProperty(100)
	hp_max = NumericProperty(100)
	hpbar_color_width = 20
	
	def change_angle(self, *args):
		ch = rnd(3)
		self.da = 90
		if ch == 1:
			self.da = -90
		elif ch == 2:
			self.da = 0
		Clock.schedule_once(self.change_angle, random.random()*1.5)
		
	
	def __init__(self,root,target):
		super(Monster,self).__init__(root)
		self.source = 'monster.png'
		self.size = (24,24)
		self.target = target
		self.root = root
		Clock.schedule_once(self.change_angle, 0.1)
		
	def move(self):
		x,y = self.target.center
		x = self.center_x - x
		y = self.center_y - y
		h = (x**2 + y**2)**0.5
		if h:
			self.angle = asin(y/h)
			if x < 0:
				self.angle = pi  - self.angle
		self.angle = self.angle * 180 / pi
		  
		self.dvx = self.v * cos(self.da * pi/ 180)
		self.dvy = self.v * sin(self.da * pi/ 180)

		self.vx = - self.v * cos(self.angle * pi/ 180)
		self.vy = - self.v * sin(self.angle * pi/ 180)
		
		self.x += self.vx
		self.y += self.vy
		self.x += self.dvx
		self.y += self.dvy

		
	def hit(self, content):
		self.hp -= content['damage'] #!!!!!!!!!!!!!
		if self.hp <= 0:
			self.kill()
	
	def kill(self):
		self.root.mutantdie.play()
		try:
			self.root.monsters.remove(self)
			Game.new_help()
		except:
			pass
		super(Monster,self).kill()
		del(self)
		
		

class Shell(Obj):
	vx = NumericProperty(0)
	vy = NumericProperty(0)
	v = NumericProperty(19)
	def __init__(self, center, angle, root):
		super(Shell,self).__init__(root)
		self.source = 'bullet.png'
		self.angle = angle+180
		self.root = root
		self.pos = center
		self.size = (8,2)
		self.vx = self.v * cos(self.angle * pi/ 180)
		self.vy = self.v * sin(self.angle * pi/ 180)
		self.root.bullets += [self]


	def move(self):
		self.x += self.vx
		self.y += self.vy
		for mon in self.root.monsters:
			if mon.collide_widget(self):
				self.kill()
				mon.hit(self.damage)
				break
				
		for b in self.root.helpers: 
			if b.collide_widget(self):
				self.kill()
				b.kill()
				break
		
	def kill(self, *args):
		try:
			self.root.bullets.remove(self)
		except:
			pass
		super(Shell,self).kill()
		del(self)


class Bullet1(Shell):
	damage = {'damage':30}
	super(Shell).__init__(Obj)
	

class Bullet2(Shell):
	damage = {'damage':80}
	super(Shell).__init__(Obj)

class Bullet3(Shell):
	damage = {'damage':20}
	super(Shell).__init__(Obj)

class Bullet4(Shell):
	damage = {'damage':40}
	super(Shell).__init__(Obj)


		
class Man(Obj):
	damage = NumericProperty(50)
	hp = NumericProperty(100)
	def __init__(self, root):
		super(Man,self).__init__(root)
		self.source = 'solder.zip'
		self.size = (48,88)
		self.vx = 6
		self.vy = 6
		self.keys = set()
		self.root = root
		self.target = Obj(self.root)
		self.target.source = 'bullet.png'
		self.weapons = {'Spas':WeaponShotgun(self.root, self),'Mauser':WeaponSniperRifle(self.root, self),'Ak74':WeaponAvtomatKalashnikova(self.root, self),'TT':WeaponTulskiiTokarev(self.root, self),'M4A1':WeaponM4_A1(self.root, self)}
		self.ammo = {"Bullet1":1, "Bullet2":0,"Bullet3":0,"Bullet4":0}
		self.weapon_order = ['Spas','Ak74','M4A1','Mauser','TT']
		self.weapon = self.weapons[self.weapon_order[0]]
		self.autofire = 'off'
		
		
	def move(self):
		if 'left' in self.keys:
			self.x -= self.vx
		if 'right' in self.keys:
			self.x += self.vx
		if 'up' in self.keys:
			self.y += self.vy
		if 'down' in self.keys:
			self.y -= self.vy
		if 'spacebar' in self.keys:
			self.next_weapon()
			self.keys.remove('spacebar')

		self.targeting()
		if self.autofire == 'on':
			self.fire()

	def next_weapon(self):
		n = self.weapon_order.index(self.weapon.name)
		n += 1
		if n == len(self.weapon_order):
			n = 0

		self.weapon = self.weapons[self.weapon_order[n]]


	def fire(self):
		
		self.weapon.fire(pos = self.center, angle = self.angle)

	def bersek(self, dt):
		self.angle += 3 
		self.fire()

	def on_touch_down(self, touch):
		self.autofire = 'on'

	def on_touch_up(self, touch):
		self.autofire = 'off'

			
	def targeting(self):
		x = Window.mouse_pos[0] - self.root.x
		y = Window.mouse_pos[1] - self.root.y
		
		self.target.center = x,y
		
		x = self.center_x - x 
		y = self.center_y - y
		h = (x**2 + y**2)**0.5
		if h:
			self.angle = asin(y/h)
			if x < 0:
				self.angle = pi  - self.angle
		self.angle = self.angle * 180 / pi  
		
		
	def hit(self, content):
		#~ self.hp -= damage
		#~ if self.hp <= 0:
			#~ print("kill")
		if 'hp' in content:
			self.hp += content['hp']
		if 'damage' in content:
			self.hp -= content['damage']
		if 'Bullet2' in content:
			self.ammo["Bullet2"] += content["Bullet2"]
		if 'Bullet1' in content:
			self.ammo["Bullet1"] += content["Bullet1"]
		if 'Bullet3' in content:
			self.ammo["Bullet3"] += content["Bullet3"]
		if 'Bullet4' in content:
			self.ammo["Bullet4"] += content["Bullet4"]
			
			
			
class Game(Widget):
	def __init__(self):
		super(Game, self).__init__()
		self.screen_layout = FloatLayout()
		self.root = RelativeLayout()
		self.music1 = SoundLoader.load('music1.ogg')
		self.root.mutantdie = SoundLoader.load('mutantdie.wav')
		self.music1.volume = 0.3
		
		self.add_widget(self.root)
		self.add_widget(self.screen_layout)

		self.root.pos = (0,0)
		self.root.size_hint = (None, None)
		self.root.pos_hint = {'x':0,'y':0}
	

		self.root.bullets = []

		rt = Obj(self.root)
		rt.source = "bg.png"
		rt.size_hint = (None,None)
		rt.size = (2000,2000)
		#rt.pos = (-100,-100)
		self.root.monsters = []
		self.root.helpers = []
		
		self.m1 = self.root.m1 = Man(self.root)
		self.m1.pos = (200,100)

		self._keyb = Window.request_keyboard(self._keyb_closed,self)
		self._keyb.bind(on_key_down  = self._on_key_down)
		self._keyb.bind(on_key_up = self._on_key_up)
		
		Clock.schedule_interval(self.move, 1 / 25.0)
		Clock.schedule_interval(self.new_mon, 30)
		Clock.schedule_interval(self.new_help, 10)
		
		Clock.schedule_interval(self.move,1/30)
		Clock.schedule_interval(self.radar,1/7)
		############################self.music1.play()
		
		
		
	def new_help(self,*args):
		content = {'Bullet1':3}
		content = {'Bullet2':1}
		content = {'Bullet3':2}
		content = {'Bullet4':2}
		m = Help(self.root, pos = (rnd(800), rnd(800)), content = content)
		
		
	def new_mon(self, *args):
		d = Monster(self.root, self.m1)
		d.pos = (rnd(100), rnd(400))
		self.root.monsters.append(d)
		
		#~ d = Mimic(self.root, self.m1)
		#~ d.pos = (rnd(100), rnd(400))
		#~ self.root.monsters.append(d)
	
	def looking(self):
		pass
		
	def scene_center(self, obj):
		
		abs_x = obj.x + self.root.x
		dx = abs_x - 600
		if dx < 0:
			dx = abs_x - 50
			if dx > 0:
				dx = 0
		if dx:
			self.root.x -= dx
			if self.root.x > 0:
				self.root.x = 0
			elif self.root.x < -self.root.width - 450:
				self.root.x = -self.root.width - 450

		abs_y = obj.y + self.root.y
		dy = abs_y - 500
		if dy < 0:
			dy = abs_y - 50
			if dy > 0:
				dy = 0
		if dy:
			self.root.y -= dy
			if self.root.y > 0:
				self.root.y = 0
			elif self.root.y < -self.root.width - 450:
				self.root.y = -self.root.width - 450
				
	def move(self, *args):
		self.m1.move()
		m1 = self.root.m1
		
		def specmove(self, *args):
			pass
			
		for mon in self.root.monsters:
			if m1.collide_widget(mon):
				m1.hit(mon.damage)
				mon.hit(m1.damage)
		
		for help in self.root.helpers:
			if help.collide_widget(m1):
				m1.hit(help.content)
				help.kill()
				
		
		for bullet in self.root.bullets:
			bullet.move()
		
		for mon in self.root.monsters:
			mon.move()
		
		self.looking()
		self.scene_center(self.m1)
		
	def radar(self, *args):
		self.screen_layout.canvas.clear()
		with self.screen_layout.canvas:
			Color(0,0.2,0.5,0.3)
			Rectangle(pos = (100,100), size = (200,200))

			Color(1,0.2,0,1)
			for mon in self.root.monsters:
				x,y = mon.pos
				x /= 10
				y /= 10
				Rectangle(pos = (100+x,100+y), size = (5,5))
				

				
	
	def _on_key_down(self,keyboard, keycode, text, modifiers):
		self.m1.keys.add(keycode[1])

	def _on_key_up(self,keyboard, keycode, *largs):
		try:
			self.m1.keys.remove(keycode[1])
		except:
			pass
	
	def _keyb_closed(*args):
		print('qu-qu')
		
		
		
class Mimic(Monster):
	damage = {'damage':100}
	vx = NumericProperty(0)
	vy = NumericProperty(0)
	v = NumericProperty(2)
	da = NumericProperty(0)
	hp = NumericProperty(500)
	
	def __init__(self,root,target):
		super(Mimic,self).__init__(root,target)
		self.source = 'granata.png'
		self.source2 = 'solder.zip'
		self.box = True
	
	def move(self):
		if not self.box:
			super(Mimic, self).move()

			
	def hit(self, damage):
		if self.box:
			self.source = self.source2
			self.box = False
		else:
			super(Mimic, self).hit(damage)
		
		
class Help(Obj):
	def __init__(self,root, pos, content):
		super(Help,self).__init__(root)
		self.source = 'granata.png'
		self.size = (24,24)
		self.root = root
		self.pos = pos
		self.content = content
		self.root.helpers.append(self)
	
	def kill(self):
		super(Help,self).kill()
		try:
			self.root.helpers.remove(self)
		except:
			pass
	
		
class Weapon(Obj):
	def __init__(self,root, owner):
		super(Weapon,self).__init__(root)
		self.name = ''
		self.owner = owner
		self.can_fire = 1
		self.time_fire = 0.05
		self.time_change_bk = 0.05
		
		
	def _can_fire(self, *args):
		self.can_fire = 1		
		#self.shotgun3 = SoundLoader.load('shotgun3.ogg')
		
		
	def test_fire(self):
		if self.can_fire:
			if self.bk > 0:
				self.can_fire = 0
				Clock.schedule_once(self._can_fire,self.time_fire)
				#self.fire_sound.play()
				return True
				
			else: # перезарядка магазина
				print('change_bk')
				print(self.owner.ammo)
				
				if self.owner.ammo[self.ammo_name] > 0:
					self.owner.ammo[self.ammo_name] -= 1
					self.bk = self.max_bk
				#print(self.bk,self.max_bk,self.all_bk)
				self.can_fire = 0
				Clock.schedule_once(self._can_fire,self.time_change_bk)
				#~ self.misfire_sound.play()            
		return False

		
class WeaponShotgun(Weapon):
	def __init__(self, root, owner):
		super(WeaponShotgun, self).__init__(root,owner)
		self.time_fire = 0.6
		self.max_bk = 9
		self.name = 'Spas'
		self.time_change_bk = 2
		self.all_bk = 0 # cколько магазинов
		self.max_bk = 6 # магазин
		self.bk = 6 # количество в магазине
		self.ammo_name = 'Bullet3'
		
		
		#self.fire_sound = SoundLoader.load('shotgun3.ogg')
		#self.misfire_sound = SoundLoader('')
	
	def fire(self, **kwargs):
		
		if self.test_fire():
			self.bk -=1
			for x in range(5):
				#print(kwargs)
				kwargs['angle'] += rnd(-3,4)
				b = Bullet3(kwargs['pos'], kwargs['angle'],self.root)


class WeaponSniperRifle(Weapon):
	def __init__(self, root, owner):
		super(WeaponSniperRifle, self).__init__(root,owner)
		self.time_fire = 1.5
		self.max_bk = 5
		self.name = 'Mauser'
		self.time_change_bk = 3
		self.all_bk = 0 # cколько магазинов
		self.max_bk = 3 # магазин
		self.bk = 3 # количество в магазине
		self.ammo_name = 'Bullet2'
		
		
		#self.fire_sound = SoundLoader.load('shotgun3.ogg')
		#self.misfire_sound = SoundLoader('')#######################################################################################kill
	
	def fire(self, **kwargs):
		
		if self.test_fire():
			self.bk -=1
		
			#print(kwargs)
			kwargs['angle'] += rnd(-3,4)
			b = Bullet2(kwargs['pos'], kwargs['angle'],self.root)
			


class WeaponTulskiiTokarev(Weapon):
	def __init__(self, root, owner):
		super(WeaponTulskiiTokarev, self).__init__(root,owner)
		self.time_fire = 0.3
		self.max_bk = 9
		self.name = 'TT'
		self.time_change_bk = 1
		self.all_bk = 9 # cколько магазинов
		self.max_bk = 9 # магазин
		self.bk = 9 # количество в магазине
		self.ammo_name = 'Bullet1'
		
		
		#self.fire_sound = SoundLoader.load('shotgun3.ogg')
		#self.misfire_sound = SoundLoader('')
	
	def fire(self, **kwargs):
		
		if self.test_fire():
			self.bk -=1
			
			#print(kwargs)
			kwargs['angle'] += rnd(-3,4)
			b = Bullet1(kwargs['pos'], kwargs['angle'],self.root)


class WeaponAvtomatKalashnikova(Weapon):
	def __init__(self, root, owner):
		super(WeaponAvtomatKalashnikova, self).__init__(root,owner)
		self.time_fire = 0.1
		self.max_bk = 5
		self.name = 'Ak74'
		self.time_change_bk = 1
		self.all_bk = 0 # cколько магазинов
		self.max_bk = 30 # магазин
		self.bk = 30 # количество в магазине
		self.ammo_name = 'Bullet4'
		
		
		#self.fire_sound = SoundLoader.load('shotgun3.ogg')
		#self.misfire_sound = SoundLoader('')
	
	def fire(self, **kwargs):
		
		if self.test_fire():
			self.bk -=1
		
			#print(kwargs)
			kwargs['angle'] += rnd(-3,4)
			b = Bullet4(kwargs['pos'], kwargs['angle'],self.root)


						
class WeaponM4_A1(Weapon):
	def __init__(self, root, owner):
		super(WeaponM4_A1, self).__init__(root, owner)
		self.time_fire = 0.05
		self.max_bk = 30
		self.name = 'M4A1'
		self.time_change_bk = 2
		self.autofire_on = 0
		self.all_bk = 0 # 
		self.max_bk = 45# магазин
		self.bk = 45 # количество в магазине
		self.ammo_name = 'Bullet1'
		
		
		#self.fire_sound = SoundLoader.load('shotgun3.ogg')
		#self.misfire_sound = SoundLoader('')
	
	def fire(self, **kwargs):
		if self.test_fire():
			self.bk -= 1
	 
			b = Bullet1(kwargs['pos'], kwargs['angle'] + rnd(-3,4),self.root)
			
	
		
			
		
	
#~ class Weapon_shotgun(Weapon):
	#~ def __init__(self,root):
		#~ super(Weapon_shotgun,self).__init__(root)
		#~ #self.shotgun3 = SoundLoader.load('shotgun3.ogg')
#~ 
	#~ def fire(self, **kwargs):
		#~ for z in range(1):
			#~ b = Bullet(kwargs['pos'], kwargs['angle'] + rnd(-3,4),self.root)
			#~ self.root.bullets += [b]
		#~ #self.shotgun3.play()
	

class MyApp(App):
	def build(self):
		self.game = Game()
		return self.game


if __name__ == '__main__':
	MyApp().run()
