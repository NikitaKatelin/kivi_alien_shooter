from kivy.clock import *
from kivy.uix.image import Image
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.scatterlayout import  ScatterLayout
#~ from kivy.uix.relativelayout import  RelativeLayout
#~ from kivy.uix.floatlayout import  FloatLayout
from kivy.core.audio import SoundLoader

from kivy.graphics import *
from kivy.lang import Builder

from kivy.animation import Animation
from kivy.core.window import Window
from kivy.properties import NumericProperty, ReferenceListProperty
from math import atan, degrees, cos, sin, asin, pi

import gc
from random import randrange as rnd
import random
import time


from bullets import *
from objects import *


		
class Weapon(Obj):
	def __init__(self,root, owner):
		super(Weapon,self).__init__(root)
		self.name = ''
		self.owner = owner
		self.can_fire = 1
		self.time_fire = 0.05
		self.time_change_bk = 0.05
		
		
	def _can_fire(self, *args):
		self.can_fire = 1		
		#self.shotgun3 = SoundLoader.load('shotgun3.ogg')
		
		
	def test_fire(self):
		if self.can_fire:
			if self.bk > 0:
				self.can_fire = 0
				Clock.schedule_once(self._can_fire,self.time_fire)
				#self.fire_sound.play()
				return True
				
			else: # перезарядка магазина
				print('change_bk')
				print(self.owner.ammo)
				
				if self.owner.ammo[self.ammo_name] > 0:
					self.owner.ammo[self.ammo_name] -= 1
					self.bk = self.max_bk
				#print(self.bk,self.max_bk,self.all_bk)
				self.can_fire = 0
				Clock.schedule_once(self._can_fire,self.time_change_bk)
				#~ self.misfire_sound.play()            
		return False

		
class WeaponShotgun(Weapon):
	def __init__(self, root, owner):
		super(WeaponShotgun, self).__init__(root,owner)
		self.time_fire = 0.6
		self.image = "spas.jpg"
		self.max_bk = 9
		self.name = 'Spas'
		self.time_change_bk = 2
		self.all_bk = 0 # cколько магазинов
		self.max_bk = 6 # магазин
		self.bk = 6 # количество в магазине
		self.ammo_name = 'Bullet3'
		
		
		#self.fire_sound = SoundLoader.load('shotgun3.ogg')
		#self.misfire_sound = SoundLoader('')
	
	def fire(self, **kwargs):
		
		if self.test_fire():
			self.bk -=1
			for x in range(10):
				#print(kwargs)
				kwargs['angle'] += rnd(-3,4)
				b = Bullet3(kwargs['pos'], kwargs['angle'],self.root, kwargs['team'])


class WeaponSniperRifle(Weapon):
	def __init__(self, root, owner):
		super(WeaponSniperRifle, self).__init__(root,owner)
		self.time_fire = 1.5
		self.image = "mouser.jpg"
		self.max_bk = 5
		self.name = 'Mauser'
		self.time_change_bk = 3
		self.all_bk = 0 # cколько магазинов
		self.max_bk = 3 # магазин
		self.bk = 3 # количество в магазине
		self.ammo_name = 'Bullet2'
		
		
		#self.fire_sound = SoundLoader.load('shotgun3.ogg')
		#self.misfire_sound = SoundLoader('')#######################################################################################kill
	
	def fire(self, **kwargs):
		
		if self.test_fire():
			self.bk -=1
		
			#print(kwargs)
			kwargs['angle'] += rnd(-3,4)
			b = Bullet2(kwargs['pos'], kwargs['angle'],self.root, kwargs['team'])
			


class WeaponTulskiiTokarev(Weapon):
	def __init__(self, root, owner):
		super(WeaponTulskiiTokarev, self).__init__(root,owner)
		self.time_fire = 0.3
		self.image = "TT.jpg"
		self.max_bk = 9
		self.name = 'TT'
		self.time_change_bk = 1
		self.all_bk = 9 # cколько магазинов
		self.max_bk = 9 # магазин
		self.bk = 9 # количество в магазине
		self.ammo_name = 'Bullet1'
		
		
		#self.fire_sound = SoundLoader.load('shotgun3.ogg')
		#self.misfire_sound = SoundLoader('')
	
	def fire(self, **kwargs):
		
		if self.test_fire():
			self.bk -=1
			
			#print(kwargs)
			kwargs['angle'] += rnd(-3,4)
			b = Bullet1(kwargs['pos'], kwargs['angle'],self.root, kwargs['team'])


class WeaponAvtomatKalashnikova(Weapon):
	def __init__(self, root, owner):
		super(WeaponAvtomatKalashnikova, self).__init__(root,owner)
		self.time_fire = 0.1
		self.image = "ak74.jpg"
		self.max_bk = 5
		self.name = 'Ak74'
		self.time_change_bk = 1
		self.all_bk = 0 # cколько магазинов
		self.max_bk = 30 # магазин
		self.bk = 30 # количество в магазине
		self.ammo_name = 'Bullet4'
		
		
		#self.fire_sound = SoundLoader.load('shotgun3.ogg')
		#self.misfire_sound = SoundLoader('')
	
	def fire(self, **kwargs):
		
		if self.test_fire():
			self.bk -=1
		
			#print(kwargs)
			kwargs['angle'] += rnd(-3,4)
			b = Bullet4(kwargs['pos'], kwargs['angle'],self.root, kwargs['team'])


						
class WeaponM4_A1(Weapon):
	def __init__(self, root, owner):
		super(WeaponM4_A1, self).__init__(root, owner)
		self.time_fire = 0.03
		self.image = "m4.jpg"
		self.max_bk = 6
		self.name = 'M4A1'
		self.time_change_bk = 2
		self.autofire_on = 0
		self.all_bk = 0 # 
		self.max_bk = 45# магазин
		self.bk = 45 # количество в магазине
		self.ammo_name = 'Bullet1'
		
		
		#self.fire_sound = SoundLoader.load('shotgun3.ogg')
		#self.misfire_sound = SoundLoader('')
	
	def fire(self, **kwargs):
		if self.test_fire():
			self.bk -= 1
	 
			b = Bullet1(kwargs['pos'], kwargs['angle'] + rnd(-3,4),self.root, kwargs['team'])
			
	
		
