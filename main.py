
from kivy.app import App
from kivy.clock import *
from kivy.uix.image import Image
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.scatterlayout import  ScatterLayout
from kivy.uix.relativelayout import  RelativeLayout
from kivy.uix.floatlayout import  FloatLayout
from kivy.core.audio import SoundLoader

from kivy.graphics import *
from kivy.lang import Builder

from kivy.core.window import Window
from kivy.properties import NumericProperty, ReferenceListProperty
from math import atan, degrees, cos, sin, asin, pi

from random import randrange as rnd
import random
import time


from live_objects import *


class Game(Widget):
    def __init__(self):
        super(Game, self).__init__()
        self.screen_layout = FloatLayout()
        self.root = RelativeLayout()
        self.music1 = SoundLoader.load('music1.ogg')
        self.root.mutantdie = SoundLoader.load('mutantdie.wav')
        self.music1.volume = 0.3

        self.radar_x = 600
        self.radar_y = 400
        self.radar_w = 200
        self.radar_h = 200

        self.add_widget(self.root)
        self.add_widget(self.screen_layout)

        self.root.pos = (0,0)
        self.root.size_hint = (None, None)
        self.root.pos_hint = {'x':0,'y':0}


        self.root.bullets = []



        rt = Obj(self.root)
        rt.source = "bg.png"
        rt.size_hint = (None,None)
        rt.size = (2000,2000)
        #rt.pos = (-100,-100)
        self.root.monsters = []
        self.root.helpers = []
        self.root.walls = []
        self.root.boxes = []


        self.m1 = self.root.m1 = Man(self.root,'human')
        self.m1.pos = (200,100)

        self._keyb = Window.request_keyboard(self._keyb_closed,self)
        self._keyb.bind(on_key_down  = self._on_key_down)
        self._keyb.bind(on_key_up = self._on_key_up)

        Clock.schedule_interval(self.move, 1 / 25.0)
        Clock.schedule_interval(self.new_mon, 10)
        Clock.schedule_interval(self.new_help, 15)
        self.new_mon()
        for x in range(30):
            self.new_wall()

        #Clock.schedule_interval(self.move,1/30)
        Clock.schedule_interval(self.radar,1/7)
        ############################self.music1.play()


    def new_wall(self,*args):
        wall = Wall(self.root, pos = (rnd(800), rnd(800)))


    def new_help(self,*args):
        pass
        # content = {'Bullet1':3}
        # content = {'Bullet2':1}
        # content = {'Bullet3':2}
        # content = {'Bullet4':2}
        # m = Help(self.root, pos = (rnd(800), rnd(800)), content = content)


    def new_mon(self, *args):


        d = Monster(self.root, self.m1,'monmon')
        d.pos = (rnd(100), rnd(400))
        self.root.monsters.append(d)

        # d = Mimic(self.root, self.m1)
        # d.pos = (rnd(100), rnd(400))
        # self.root.monsters.append(d)

    def looking(self):
        pass

    def scene_center(self, obj):

        abs_x = obj.x + self.root.x
        dx = abs_x - 600
        if dx < 0:
            dx = abs_x - 50
            if dx > 0:
                dx = 0
        if dx:
            self.root.x -= dx
            if self.root.x > 0:
                self.root.x = 0
            elif self.root.x < -self.root.width - 450:
                self.root.x = -self.root.width - 450

        abs_y = obj.y + self.root.y
        dy = abs_y - 500
        if dy < 0:
            dy = abs_y - 50
            if dy > 0:
                dy = 0
        if dy:
            self.root.y -= dy
            if self.root.y > 0:
                self.root.y = 0
            elif self.root.y < -self.root.width - 450:
                self.root.y = -self.root.width - 450

    def move(self, *args):
        self.m1.move()
        m1 = self.root.m1

        def specmove(self, *args):
            pass

        for mon in self.root.monsters:
            if m1.collide_widget(mon):
                m1.hit(mon.damage)
                mon.hit({'damage':m1.damage})

        for box in self.root.boxes:
            if m1.collide_widget(box):
                if not m1.weapons[box.weapon.name]:
                    m1.weapons[box.weapon.name] = box.weapon

                box.kill()



        for help in self.root.helpers:
            if help.collide_widget(m1):
                m1.hit(help.content)
                help.kill()

        # for wall in self.root.walls:
        #     if wall.collide_widget(m1):
        #         m1.hit(help.content)


        for bullet in self.root.bullets:
            bullet.move()

            for mon in self.root.monsters:
                if bullet.team != mon.team and  mon.collide_widget(bullet):
                    bullet.penetration -= 1
                    print(bullet.penetration)
                    if bullet.penetration <= 0:
                        print(bullet.penetration)
                        bullet.kill()
                    mon.hit(bullet.damage)
                    break

            for box in self.root.helpers:
                if box.collide_widget(bullet):
                    bullet.kill()
                    box.kill()
                    break



        for mon in self.root.monsters:
            mon.move()

        self.looking()
        self.scene_center(self.m1)

    def radar(self, *args):
        self.screen_layout.canvas.clear()
        with self.screen_layout.canvas:
            Color(0,0.2,0.5,0.3)
            Rectangle(pos = (self.radar_x,self.radar_y), size = (self.radar_w,self.radar_h))

            Color(1,0.2,0,1)
            for mon in self.root.monsters:
                x,y = mon.pos
                x /= 10
                y /= 10
                Rectangle(pos = (self.radar_x+x,self.radar_y+y), size = (5,5))

            Color(1,1,0,1)
            for help in self.root.helpers:
                x,y = help.pos
                x /= 10
                y /= 10
                Rectangle(pos = (self.radar_x+x,self.radar_y+y), size = (5,5))

            Color(0,1,0,1)
            x,y = self.m1.pos
            x /= 10
            y /= 10
            Rectangle(pos = (self.radar_x+x,self.radar_y+y), size = (5,5))


    def _on_key_down(self,keyboard, keycode, text, modifiers):
        self.m1.keys.add(keycode[1])

    def _on_key_up(self,keyboard, keycode, *largs):
        try:
            self.m1.keys.remove(keycode[1])
        except:
            pass

    def _keyb_closed(*args):
        print('qu-qu')


class MyApp(App):
    def build(self):
        self.game = Game()
        return self.game


if __name__ == '__main__':
    MyApp().run()
