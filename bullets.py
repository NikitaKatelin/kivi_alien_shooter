from kivy.app import App
from kivy.clock import *
from kivy.uix.image import Image
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.scatterlayout import ScatterLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.core.audio import SoundLoader

from kivy.graphics import *
from kivy.lang import Builder

from kivy.animation import Animation
from kivy.core.window import Window
from kivy.properties import NumericProperty, ReferenceListProperty
from math import atan, degrees, cos, sin, asin, pi

from random import randrange as rnd
import random
import time

from objects import *


class Shell(Obj):
    vx = NumericProperty(0)
    vy = NumericProperty(0)
    v = NumericProperty(19)

    def __init__(self, center, angle, root, team):
        super(Shell, self).__init__(root)
        self.source = 'bullet.png'
        self.angle = angle + 180
        self.root = root
        self.penetration = 1
        self.pos = center
        self.size = (8, 2)
        self.vx = self.v * cos(self.angle * pi / 180)
        self.vy = self.v * sin(self.angle * pi / 180)
        self.root.bullets += [self]

        self.team = team


    def move(self):
        self.x += self.vx
        self.y += self.vy


    def kill(self, *args):
        try:
            self.root.bullets.remove(self)
        except:
            pass
        super(Shell, self).kill()
        del (self)


class Bullet1(Shell):
    def __init__(self, pos, angle, root, team):
        print('!!!+++')
        super(Bullet1, self).__init__(pos, angle, root, team,)
        print('!!!---')
        self.damage = {'damage': 30}
        self.penetration = 1


class Bullet2(Shell):
    def __init__(self, pos, angle, root, team):
        print('!!!+++')
        super(Bullet2, self).__init__(pos, angle, root, team)
        print('!!!---')
        self.damage = {'damage': 80}
        self.penetration = 5


class Bullet3(Shell):
    def __init__(self, pos, angle, root, team):
        print('!!!+++')
        super(Bullet3, self).__init__(pos, angle, root, team)
        print('!!!---')
        self.damage = {'damage': 20}
        self.penetration = 1


class Bullet4(Shell):
    def __init__(self, pos, angle, root, team):
        print('!!!+++')
        super(Bullet4, self).__init__(pos, angle, root, team)
        print('!!!---')
        self.damage = {'damage': 45}
        self.penetration = 2


