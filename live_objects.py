from kivy.clock import *
from kivy.uix.image import Image
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.scatterlayout import ScatterLayout
# ~ from kivy.uix.relativelayout import  RelativeLayout
#~ from kivy.uix.floatlayout import  FloatLayout
from kivy.core.audio import SoundLoader

from kivy.graphics import *
from kivy.lang import Builder

from kivy.animation import Animation
from kivy.core.window import Window
from kivy.properties import NumericProperty, ReferenceListProperty
from math import atan, degrees, cos, sin, asin, pi

import gc
from random import randrange as rnd
import random
import time
from weapons import *
from objects import *


class Monster(Obj):
    damage = {'damage': 25}
    vx = NumericProperty(0)
    vy = NumericProperty(0)
    v = NumericProperty(1.5)
    da = NumericProperty(0)
    hp = NumericProperty(100)
    hp_max = NumericProperty(100)
    hpbar_color_width = 20

    def __init__(self, root, target, team):
        super(Monster, self).__init__(root)
        self.source = 'monster.png'
        self.size = (24, 24)
        self.target = target
        self.root = root
        self.team = team

        self.weapons = {'Spas': WeaponShotgun(self.root, self), 'Mauser': WeaponSniperRifle(self.root, self),
                        'Ak74': WeaponAvtomatKalashnikova(self.root, self), 'TT': WeaponTulskiiTokarev(self.root, self),
                        'M4A1': WeaponM4_A1(self.root, self)}
        self.ammo = {"Bullet1": 1, "Bullet2": 0, "Bullet3": 0, "Bullet4": 0}
        self.weapon_order = ['Spas', 'Ak74', 'M4A1', 'Mauser', 'TT']
        self.weapon = self.weapons[self.weapon_order[3]]
        self.autofire = 'on'

        Clock.schedule_once(self.change_angle, 0.1)


    def change_angle(self, *args):
        ch = rnd(3)
        self.da = 90
        if ch == 1:
            self.da = -90
        elif ch == 2:
            self.da = 0
        Clock.schedule_once(self.change_angle, random.random() * 1.5)


    def move(self):
        '''
        x, y = self.target.center
        x = self.center_x - x
        y = self.center_y - y
        h = (x ** 2 + y ** 2) ** 0.5
        if h:
            self.angle = asin(y / h)
            if x < 0:
                self.angle = pi - self.angle
        self.angle = self.angle * 180 / pi
        '''

        self.dvx = self.v * cos(self.da * pi / 180)
        self.dvy = self.v * sin(self.da * pi / 180)

        self.vx = - self.v * cos(self.angle * pi / 180)
        self.vy = - self.v * sin(self.angle * pi / 180)

        self.x += self.vx
        self.y += self.vy
        self.x += self.dvx
        self.y += self.dvy
        self.targeting()

        if self.autofire == 'on':
            self.weapon.fire(pos=self.center, angle=self.angle, team = self.team)


    def next_weapon(self):
        n = self.weapon_order.index(self.weapon.name)
        n += 1
        if n == len(self.weapon_order):
            n = 0

        self.weapon = self.weapons[self.weapon_order[n]]
        print(self.weapon.name)



    def targeting(self):
        x = self.target.center_x - self.root.x
        y = self.target.center_y - self.root.y

        x = self.center_x - x
        y = self.center_y - y
        h = (x ** 2 + y ** 2) ** 0.5
        if h:
            self.angle = asin(y / h)
            if x < 0:
                self.angle = pi - self.angle
        self.angle = self.angle * 180 / pi



    def hit(self, content):
        self.hp -= content['damage']  #!!!!!!!!!!!!!
        if self.hp <= 0:
            self.kill()

    def kill(self):
        self.root.mutantdie.play()
        ObjWeapon(self.root,self.center,self.weapon)
        try:
            self.root.monsters.remove(self)
            Game.new_help()
        except:
            pass
        super(Monster, self).kill()
        del (self)


class Man(Obj):
    damage = NumericProperty(50)
    hp = NumericProperty(100)

    def __init__(self, root, team):
        super(Man, self).__init__(root)
        self.source = 'solder.zip'
        self.size = (48, 88)
        self.vx = 6
        self.vy = 6
        self.keys = set()
        self.root = root
        self.team = team
        self.target = Obj(self.root)
        self.target.source = 'bullet.png'
        self.weapons = {'Spas': WeaponShotgun(self.root, self), 'Mauser': '',
                        'Ak74': WeaponAvtomatKalashnikova(self.root, self), 'TT': '',
                        'M4A1': WeaponM4_A1(self.root, self)}
        '''
        self.weapons = {'Spas': WeaponShotgun(self.root, self), 'Mauser': WeaponSniperRifle(self.root, self),
                        'Ak74': WeaponAvtomatKalashnikova(self.root, self), 'TT': WeaponTulskiiTokarev(self.root, self),
                        'M4A1': WeaponM4_A1(self.root, self)}
        '''
        self.ammo = {"Bullet1": 1, "Bullet2": 0, "Bullet3": 0, "Bullet4": 0}
        self.weapon_order = ['Spas', 'Ak74', 'M4A1', 'Mauser', 'TT']
        self.weapon = self.weapons[self.weapon_order[0]]
        self.autofire = 'off'


    def move(self):
        if 'left' in self.keys:
            self.x -= self.vx

        if 'right' in self.keys:
            self.x += self.vx

        if 'up' in self.keys:
            self.y += self.vy
        if 'down' in self.keys:
            self.y -= self.vy
        if 'spacebar' in self.keys:
            self.next_weapon()
            self.keys.remove('spacebar')

        self.targeting()
        if self.autofire == 'on':
            self.fire()

    def next_weapon(self):
        n = self.weapon_order.index(self.weapon.name)
        nn = n
        n += 1
        if n == len(self.weapon_order):
            n = 0


        # while not self.weapons[self.weapon_order[n]] or self.weapons[self.weapon_order[n]].ammo == 0 and not self.ammo[self.weapon_order[n]] and nn != n:


        while not self.weapons[self.weapon_order[n]] or self.weapons[self.weapon_order[n]].bk == 0 and not self.ammo[self.weapons[self.weapon_order[n]].ammo_name] == 0 and n != nn:
            n += 1
            if n == len(self.weapon_order):
                n = 0

        self.weapon = self.weapons[self.weapon_order[n]]



    def fire(self):
        self.weapon.fire(pos=self.center, angle=self.angle, team = self.team)

    def bersek(self, dt):
        self.angle += 3
        self.fire()

    def on_touch_down(self, touch):
        self.autofire = 'on'

    def on_touch_up(self, touch):
        self.autofire = 'off'


    def targeting(self):
        x = Window.mouse_pos[0] - self.root.x
        y = Window.mouse_pos[1] - self.root.y

        self.target.center = x, y

        x = self.center_x - x
        y = self.center_y - y
        h = (x ** 2 + y ** 2) ** 0.5
        if h:
            self.angle = asin(y / h)
            if x < 0:
                self.angle = pi - self.angle
        self.angle = self.angle * 180 / pi


    def hit(self, content):
        #~ self.hp -= damage
        #~ if self.hp <= 0:
        #~ print("kill")

        if 'damage' in content:
             self.hp -= content['damage']
        if 'damage' in content:
            self.hp -= content['damage']
        if 'Bullet2' in content:
            self.ammo["Bullet2"] += content["Bullet2"]
        if 'Bullet1' in content:
            self.ammo["Bullet1"] += content["Bullet1"]
        if 'Bullet3' in content:
            self.ammo["Bullet3"] += content["Bullet3"]
        if 'Bullet4' in content:
            self.ammo["Bullet4"] += content["Bullet4"]
        if self.hp < 0:
            self.y = 0
            self.x = 0
            self.ammo = {"Bullet1": 0, "Bullet2": 0, "Bullet3": 0, "Bullet4": 0}
            self.hp = 100


class Mimic(Monster):
    damage = {'damage': 100}
    vx = NumericProperty(0)
    vy = NumericProperty(0)
    v = NumericProperty(2)
    da = NumericProperty(0)
    hp = NumericProperty(500)

    def __init__(self, root, target):
        super(Mimic, self).__init__(root, target)
        self.source = 'granata.png'
        self.source2 = 'solder.zip'
        self.box = True

    def move(self):
        if not self.box:
            super(Mimic, self).move()


    def hit(self, damage):
        if self.box:
            self.source = self.source2
            self.box = False
        else:
            super(Mimic, self).hit(damage)
class Wall(Obj):
    def __init__(self, root, pos):
        super(Wall, self).__init__(root)
        self.source = 'granata.png'
        self.size = (30, 30)
        self.root = root
        self.pos = pos
        self.root.walls.append(self)



class Help(Obj):
    def __init__(self, root, pos, content):
        super(Help, self).__init__(root)
        self.source = '56.PNG'
        self.size = (22, 22)
        self.root = root
        self.pos = pos
        self.content = content
        self.root.helpers.append(self)

    def kill(self):
        super(Help, self).kill()
        try:
            self.root.helpers.remove(self)
        except:
            pass


class ObjWeapon(Obj):
    def __init__(self, root, pos, weapon):
        super(ObjWeapon, self).__init__(root)
        self.source = weapon.image
        self.pos = pos
        self.size = (20,20)
        root.boxes.append(self)
        self.weapon = weapon

    def kill(self):
        super(ObjWeapon, self).kill()
        try:
            self.root.boxes.remove(self)
        except:
            pass
