
from kivy.clock import *
from kivy.uix.image import Image
from kivy.uix.widget import Widget
from kivy.uix.button import Button
from kivy.uix.scatterlayout import  ScatterLayout
#~ from kivy.uix.relativelayout import  RelativeLayout
#~ from kivy.uix.floatlayout import  FloatLayout
from kivy.core.audio import SoundLoader

from kivy.graphics import *
from kivy.lang import Builder

from kivy.animation import Animation
from kivy.core.window import Window
from kivy.properties import NumericProperty, ReferenceListProperty
from math import atan, degrees, cos, sin, asin, pi

import gc
from random import randrange as rnd
import random
import time

#from weapons import *

Builder.load_string('''
<Obj>:
	canvas.before:
		PushMatrix
		Rotate:
			angle: self.angle
			axis: (0, 0, 1)
			origin: self.center
	canvas.after:
		PopMatrix

<Monster>:
    canvas.after:
        Color:
            rgba: (1.5-self.hp/self.hp_max,self.hp/self.hp_max,0,0.3)
        Rectangle
            pos: (self.center_x-self.hpbar_color_width/2, self.y + self.height*0.8)
            size: (self.hpbar_color_width*self.hp/self.hp_max,5)
 ''')


class Obj(Image):

	angle = NumericProperty(0)
	def __init__(self, root):
		super(Obj,self).__init__()
		self.root = root
		self.size_hint = (None,None)
		root.add_widget(self)
	   
	def kill(self, *args):
		self.root.remove_widget(self)
		del(self)
		
